package com.effectivemobile.test;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Contact;
import io.swagger.v3.oas.annotations.info.Info;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@OpenAPIDefinition(
        info = @Info(
                title = "Comment microservice",
                description = "API providing access to comments information", version = "1.0.0",
                contact = @Contact(
                        name = "Artem Aldungarov",
                        url = "https://gitlab.com/com.effectivemobile.test/tasker",
                        email = "aldungarov.ar@gmail.com"
                )
        )
)
@SpringBootApplication
@ConfigurationPropertiesScan
@Configuration
@EnableAspectJAutoProxy
public class CommentMicroserviceApp {
    public static void main(String[] args) {
        SpringApplication.run(CommentMicroserviceApp.class, args);
    }
}