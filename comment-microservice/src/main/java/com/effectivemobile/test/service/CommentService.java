package com.effectivemobile.test.service;

import com.effectivemobile.test.dto.CommentRq;
import com.effectivemobile.test.dto.CommentRs;
import com.effectivemobile.test.dto.CommonRs;
import com.effectivemobile.test.entity.Comment;
import com.effectivemobile.test.exception.BadRequestException;
import com.effectivemobile.test.exception.CommentNotFoundException;
import com.effectivemobile.test.repository.CommentRepository;
import com.effectivemobile.test.utils.CommentMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor
public class CommentService {

    private final CommentRepository commentRepository;
    private final CommentMapper commentMapper;

    public CommonRs<CommentRs> getCommentById(Long id) {
        Comment comment = extractCommentFromDbById(id);

        return new CommonRs<>(commentMapper.toCommentRs(comment));
    }

    public CommonRs<CommentRs> updateCommentInfo(Long id, CommentRq commentRq) {

        validateCommentUpdateRequest(commentRq);

        Comment comment = extractCommentFromDbById(id);

        if (commentRq.getCommentText() != null) {
            comment.setCommentText(commentRq.getCommentText());
        }

        commentRepository.save(comment);

        return new CommonRs<>(commentMapper.toCommentRs(comment));
    }

    public CommonRs<String> deleteComment(Long id) {

        Comment comment = extractCommentFromDbById(id);

        commentRepository.delete(comment);

        return new CommonRs<>("Comment with id " + id + " successfully deleted");
    }

    public CommonRs<CommentRs> createComment(CommentRq commentRq) {

        validateCommentCreateRequest(commentRq);

        Comment comment = commentMapper.toComment(commentRq);
        comment.setCreatedAt(LocalDateTime.now());

        Comment savedComment = commentRepository.save(comment);

        return new CommonRs<>(commentMapper.toCommentRs(savedComment));
    }

    private void validateCommentCreateRequest(CommentRq commentRq) {

        if (commentRq == null || commentRq.isEmpty()) {
           throw new BadRequestException("Comment create request body should not be empty");
        }

        String errorMessage = "Task create request validation failed: ";
        boolean validationFailed = false;

        if (commentRq.getCommentText() == null || commentRq.getCommentText().isEmpty()) {
            errorMessage += "| Comment text can not be empty |";
            validationFailed = true;
        }

        if (commentRq.getAuthorId() == null || commentRq.getAuthorId().isEmpty()) {
            errorMessage += "| Author id can not be empty |";
            validationFailed = true;
        }

        if (commentRq.getTaskId() == null || commentRq.getTaskId().isEmpty()) {
            errorMessage += "| Task id can not be empty |";
            validationFailed = true;
        }

        if (validationFailed) {
            throw new BadRequestException(errorMessage);
        }
    }

    private void validateCommentUpdateRequest(CommentRq commentRq) {

        if (commentRq == null || commentRq.isEmpty()) {
            throw new BadRequestException("Comment update request body is empty");
        }

        if (commentRq.getCommentText() == null || commentRq.getCommentText().isEmpty()) {
            throw new BadRequestException("Comment text can not be empty");
        }
    }

    private Comment extractCommentFromDbById(Long id) {

        return commentRepository.findById(id)
                .orElseThrow(() -> new CommentNotFoundException("Comment with id " + id + " not found"));
    }
}
