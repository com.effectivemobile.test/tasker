package com.effectivemobile.test.exception;

public class CommentNotFoundException extends BadRequestException {

    public CommentNotFoundException(String message) {
        super(message);
    }
}
