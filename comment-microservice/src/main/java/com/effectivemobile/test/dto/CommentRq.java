package com.effectivemobile.test.dto;

import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Schema(description = "Change comment info request")
public class CommentRq {

    @Schema(example = "Some text")
    private String commentText;

    @Schema(example = "141")
    private String taskId;

    @Schema(example = "007")
    private String authorId;

    public boolean isEmpty() {

        return (commentText == null || commentText.isEmpty()) &&
                (authorId == null || authorId.isEmpty()) &&
                (taskId == null || taskId.isEmpty());
    }
}
