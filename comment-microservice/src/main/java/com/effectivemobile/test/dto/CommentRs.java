package com.effectivemobile.test.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommentRs {

    @Schema(example = "1")
    private Long id;

    @Schema(example = "Good morning everyone")
    private String commentText;

    @Schema(example = "141")
    private String taskId;

    @Schema(example = "2021-08-01T00:00:00")
    private String createdAt;

    @Schema(example = "007")
    private String authorId;
}
