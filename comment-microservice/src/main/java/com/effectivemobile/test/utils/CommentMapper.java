package com.effectivemobile.test.utils;

import com.effectivemobile.test.dto.CommentRq;
import com.effectivemobile.test.dto.CommentRs;
import com.effectivemobile.test.entity.Comment;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.factory.Mappers;

@SuppressWarnings("unused")
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface CommentMapper {

    CommentMapper INSTANCE = Mappers.getMapper(CommentMapper.class);

    @Mapping(target = "createdAt", source = "createdAt", dateFormat = "yyyy-MM-dd'T'HH:mm:ss")
    @Mapping(target = "authorId", source = "authorId")
    CommentRs toCommentRs(Comment comment);

    Comment toComment(CommentRq commentRq);
}
