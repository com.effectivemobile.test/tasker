package com.effectivemobile.test.service;

import com.effectivemobile.test.dto.AuthResult;
import com.effectivemobile.test.dto.UserCredentials;
import com.effectivemobile.test.exception.AuthResponseParseException;
import com.effectivemobile.test.exception.AuthenticationException;
import com.effectivemobile.test.kafka.KafkaMessage;
import com.effectivemobile.test.kafka.KafkaService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.concurrent.ExecutionException;

@Service
@RequiredArgsConstructor
@Slf4j
public class AuthService {

    @Value("${spring.kafka.gateway-to-user-topic}")
    private String gatewayToUserTopic;
    private final KafkaService kafkaService;
    private final ObjectMapper objectMapper;

    public UserCredentials authenticate(UserCredentials userCredentials)
            throws ExecutionException, InterruptedException, JsonProcessingException {

        userCredentials.setPassword(getEncodedPassword(userCredentials.getPassword()));

        KafkaMessage kafkaMessage = new KafkaMessage("authenticate",
                objectMapper.writeValueAsString(userCredentials));
        String response = kafkaService.sendMessage(gatewayToUserTopic, kafkaMessage);
        userCredentials.setPassword(null);

        checkAuthenticationResult(response);

        return readValueWithMapper(response.split(";")[1], UserCredentials.class);
    }

    private void checkAuthenticationResult(String response) {

        String authResult = response.split(";")[0];

        if (authResult == null || authResult.isEmpty()) {
            throw new AuthenticationException("Authentication operation result empty");
        }

        if (authResult.equals("EXCEPTION")) {
            throw new AuthenticationException("Authentication operation result unknown");
        }

        if (authResult.equals("FAIL")) {
            throw new AuthenticationException("Wrong email or password");
        } else if (authResult.equals("SUCCESS") &&
                (response.split(";").length != 2 ||
                    response.split(";")[1] == null ||
                    response.split(";")[1].isEmpty())) {
            log.error("Authentication operation result unknown: " + response + " " + response.split(",").length + authResult);
                throw new AuthenticationException("Authentication operation result unknown");
        } else if (authResult.equals("SUCCESS")) {
            return;
        }

        log.error("Authentication check failed: " + response + " " + authResult);
        throw new AuthenticationException("Authentication operation result unknown");
    }

    public String getEncodedPassword(String password) {
        byte[] encodedBytes = Base64.getEncoder().encode(password.getBytes());
        return new String(encodedBytes);
    }

    private <T> T readValueWithMapper(String input, Class<T> objectClass) {
        try {
            return objectMapper.readValue(input, objectClass);
        } catch (JsonProcessingException e) {
            throw new AuthResponseParseException("Failed to parse auth response");
        }
    }
}
