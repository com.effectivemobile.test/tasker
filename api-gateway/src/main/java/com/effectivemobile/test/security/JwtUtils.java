package com.effectivemobile.test.security;

import com.effectivemobile.test.annotation.Debug;
import com.effectivemobile.test.dto.UserCredentials;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

@Component
@RequiredArgsConstructor
@Debug
@Slf4j
public class JwtUtils {

    @Value("${jwt.secret}")
    private String secret;

    @Value("${jwt.expiration-term-hours}")
    private String expirationTermHours;

    private static final Long MILLIS_HOURS_MULTIPLIER = 3_600_000L;

    public String generateToken(UserCredentials userCredentials) {
        Map<String, Object> claims = new HashMap<>();
        claims.put("roles", "ROLE_USER");
        claims.put("id", userCredentials.getId());
        return createToken(claims, userCredentials.getUsername());
    }

    private String createToken(Map<String, Object> claims, String username) {
        return Jwts.builder()
                .setClaims(claims)
                .setSubject(username)
                .setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(calculateExpirationDate())
                .signWith(SignatureAlgorithm.HS256, secret)
                .compact();
    }

    public Authentication getAuthentication(String token) {
        return new UsernamePasswordAuthenticationToken(
                extractUsername(token),
                null,
                extractRoles(token)
                        .stream()
                        .map(SimpleGrantedAuthority::new)
                        .toList()
        );
    }

    public List<String> extractRoles(String token) {

        //noinspection unchecked
        return getClaims(token).get("roles", List.class);
    }

    public Long getExpirationTermHours() {
        return Long.parseLong(expirationTermHours);
    }

    public Boolean validateToken(String token, UserCredentials userCredentials) {
        final String username = extractUsername(token);

        return (username.equals(userCredentials.getUsername()) && !isTokenExpired(token));
    }

    public String extractUsername(String token) {
        return getClaims(token).getSubject();
    }

    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {

        final Claims claims = getClaims(token);
        return claimsResolver.apply(claims);
    }

    private Claims extractAllClaims(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJwt(token).getBody();
    }

    private Claims getClaims(String token) {

        return Jwts.parser()
                .setSigningKey(secret)
                .parseClaimsJws(token)
                .getBody();
    }

    private Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    private Date calculateExpirationDate() {
        return new Date(System.currentTimeMillis() + (getExpirationTermHours() * MILLIS_HOURS_MULTIPLIER));
    }
}
