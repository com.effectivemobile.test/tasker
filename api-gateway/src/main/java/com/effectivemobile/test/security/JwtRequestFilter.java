package com.effectivemobile.test.security;

import com.effectivemobile.test.dto.UserCredentials;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;

@Component
@RequiredArgsConstructor
public class JwtRequestFilter extends OncePerRequestFilter {

    private final JwtUtils jwtUtils;
    private final UserDetailsServiceImpl userDetailsService;

    @SuppressWarnings("NullableProblems")
    @Override
    protected void doFilterInternal(HttpServletRequest request,
                                    HttpServletResponse response,
                                    FilterChain chain) throws ServletException, IOException {

        final String authorizationHeader = request.getHeader("Authorization");

        String username = null;

        if (authorizationHeader != null && authorizationHeader.length() > 7) {
            username = jwtUtils.extractUsername(authorizationHeader);
        }

        if (username != null && SecurityContextHolder.getContext().getAuthentication() == null) {

            UserCredentials userDetails;
            userDetails = (UserCredentials) userDetailsService.loadUserByUsername(username);

            if (Boolean.FALSE.equals(jwtUtils.validateToken(authorizationHeader, userDetails))) {
                chain.doFilter(request, response);
                return;
            }

            setAuthenticationContext(authorizationHeader);
            chain.doFilter(request, response);
        }
        chain.doFilter(request, response);
    }

    private void setAuthenticationContext(String token) {
        Authentication authentication = jwtUtils.getAuthentication(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }

}
