package com.effectivemobile.test.security;

import com.effectivemobile.test.dto.UserCredentials;
import com.effectivemobile.test.exception.AuthResponseParseException;
import com.effectivemobile.test.exception.AuthenticationException;
import com.effectivemobile.test.exception.UserNotFoundException;
import com.effectivemobile.test.kafka.KafkaMessage;
import com.effectivemobile.test.kafka.KafkaService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;

import java.util.concurrent.ExecutionException;

@Service
@RequiredArgsConstructor
public class UserDetailsServiceImpl implements UserDetailsService {

    private final KafkaService kafkaService;
    private final ObjectMapper objectMapper;
    @Value("${spring.kafka.gateway-to-user-topic}")
    private String gatewayToUserTopic;

    @Override
    public UserDetails loadUserByUsername(String username) {

        KafkaMessage kafkaMessage = new KafkaMessage("getCredentialsByUserName", username);
        String response;
        try {
            response = kafkaService.sendMessage(gatewayToUserTopic, kafkaMessage);
        } catch (ExecutionException | InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new AuthenticationException("User credentials load failed");
        }

        if (response == null || response.isEmpty()) {
            throw new AuthenticationException("User credentials load failed");
        }

        KafkaMessage responseMessage;
        try {
            responseMessage = objectMapper.readValue(response, KafkaMessage.class);
        } catch (JsonProcessingException e) {
            throw new AuthResponseParseException("User credentials parsing error");
        }

        if (responseMessage.getOperation() != null &&
                (responseMessage.getOperation().equals("EXCEPTION") ||
                        responseMessage.getMessage().equals("not found"))) {
            throw new UserNotFoundException("User not found");
        }

        if (responseMessage.getMessage() == null || responseMessage.getMessage().isEmpty()) {
            throw new AuthenticationException("User credentials load failed");
        }

        UserCredentials userCredentials;
        try {
            userCredentials = objectMapper.readValue(responseMessage.getMessage(), UserCredentials.class);
        } catch (JsonProcessingException e) {
            throw new AuthenticationException("User credentials parsing error");
        }

        return userCredentials;
    }
}

