package com.effectivemobile.test.controller;

import com.effectivemobile.test.annotation.swagger.FullSwaggerDescription;
import com.effectivemobile.test.annotation.swagger.IdParamInfo;
import com.effectivemobile.test.kafka.KafkaMessage;
import com.effectivemobile.test.kafka.KafkaService;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/v1/task")
@RequiredArgsConstructor
@Tag(name = " Controller",
        description = "CRUD operations with task")
@ApiResponse(responseCode = "200")
public class TaskController {

    private final KafkaService kafkaService;

    @Value("${spring.kafka.gateway-to-task-topic}")
    private String gatewayToTopic;

    @ApiResponse(responseCode = "200",
            content = @Content(examples = @ExampleObject(value = "{\"status\": \"UP\"}")))
    @GetMapping(value = "/health", produces = "application/json")
    public String getHealthStatus() throws ExecutionException, InterruptedException {

        KafkaMessage taskKafkaMessage = new KafkaMessage();
        taskKafkaMessage.setOperation("health");
        return kafkaService.sendMessage(gatewayToTopic, taskKafkaMessage);
    }

    @FullSwaggerDescription(summary = "Get task by id")
    @GetMapping(value = "/{id}", produces = "application/json")
    public String getById(@PathVariable(value = "id") @IdParamInfo Long id)
            throws ExecutionException, InterruptedException {

        KafkaMessage taskKafkaMessage = new KafkaMessage();
        taskKafkaMessage.setOperation("getTaskById");
        taskKafkaMessage.setTargetId(id.toString());

        return kafkaService.sendMessage(gatewayToTopic, taskKafkaMessage);
    }

    @FullSwaggerDescription(summary = "Change tasks info")
    @PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    public String updateInfo(@PathVariable(value = "id") @IdParamInfo Long id,
                                 @RequestBody String taskRq) throws ExecutionException, InterruptedException {

        KafkaMessage taskKafkaMessage = new KafkaMessage("updateTaskInfo", taskRq);
        taskKafkaMessage.setTargetId(id.toString());
        return kafkaService.sendMessage(gatewayToTopic, taskKafkaMessage);
    }


    @FullSwaggerDescription(summary = "Delete task by id")
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public String delete(@PathVariable(value = "id") @IdParamInfo Long id) throws ExecutionException, InterruptedException {

        KafkaMessage taskKafkaMessage = new KafkaMessage("deleteTask", null);
        taskKafkaMessage.setTargetId(id.toString());
        return kafkaService.sendMessage(gatewayToTopic, taskKafkaMessage);
    }

    @FullSwaggerDescription(summary = "Create new task")
    @PostMapping(consumes = "application/json", produces = "application/json")
    public String create(@RequestBody String taskRq) throws ExecutionException, InterruptedException {

        KafkaMessage taskKafkaMessage = new KafkaMessage("createTask", taskRq);
        return kafkaService.sendMessage(gatewayToTopic, taskKafkaMessage);
    }
}
