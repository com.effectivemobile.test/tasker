package com.effectivemobile.test.controller;

import com.effectivemobile.test.annotation.swagger.FullSwaggerDescription;
import com.effectivemobile.test.annotation.swagger.IdParamInfo;
import com.effectivemobile.test.kafka.KafkaService;
import com.effectivemobile.test.kafka.KafkaMessage;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/v1/user")
@RequiredArgsConstructor
@Tag(name = "User Controller",
        description = "CRUD operations with user")
@ApiResponse(responseCode = "200")
public class UserController {

    private final KafkaService kafkaService;

    @Value("${spring.kafka.gateway-to-user-topic}")
    private String gatewayToUserTopic;

    @ApiResponse(responseCode = "200",
            content = @Content(examples = @ExampleObject(value = "{\"status\": \"UP\"}")))
    @GetMapping(value = "/health", produces = "application/json")
    public String getHealthStatus() throws ExecutionException, InterruptedException {

        KafkaMessage kafkaMessage = new KafkaMessage();
        kafkaMessage.setOperation("health");
        return kafkaService.sendMessage(gatewayToUserTopic, kafkaMessage);
    }

    @FullSwaggerDescription(summary = "Get user by id")
    @GetMapping(value = "/{id}", produces = "application/json")
    public String getUserById(@PathVariable(value = "id") @IdParamInfo Long id)
            throws ExecutionException, InterruptedException {

        KafkaMessage kafkaMessage = new KafkaMessage();
        kafkaMessage.setOperation("getUserById");
        kafkaMessage.setTargetId(id.toString());

        return kafkaService.sendMessage(gatewayToUserTopic, kafkaMessage);
    }

    @FullSwaggerDescription(summary = "Change users info")
    @PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    public String updateUserInfo(@PathVariable(value = "id") @IdParamInfo Long id,
                                 @RequestBody String userRq) throws ExecutionException, InterruptedException {

        KafkaMessage kafkaMessage = new KafkaMessage("updateUserInfo", userRq);
        kafkaMessage.setTargetId(id.toString());
        return kafkaService.sendMessage(gatewayToUserTopic, kafkaMessage);
    }


    @FullSwaggerDescription(summary = "Delete user by id")
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public String deleteUser(@PathVariable(value = "id") @IdParamInfo Long id) throws ExecutionException, InterruptedException {

        KafkaMessage kafkaMessage = new KafkaMessage("deleteUser", null);
        kafkaMessage.setTargetId(id.toString());
        return kafkaService.sendMessage(gatewayToUserTopic, kafkaMessage);
    }

    @FullSwaggerDescription(summary = "Create new user")
    @PostMapping(consumes = "application/json", produces = "application/json")
    public String createUser(@RequestBody String userRq) throws ExecutionException, InterruptedException {

        KafkaMessage kafkaMessage = new KafkaMessage("createUser", userRq);
        return kafkaService.sendMessage(gatewayToUserTopic, kafkaMessage);
    }
}
