package com.effectivemobile.test.controller;

import com.effectivemobile.test.annotation.swagger.FullSwaggerDescription;
import com.effectivemobile.test.annotation.swagger.IdParamInfo;
import com.effectivemobile.test.kafka.KafkaMessage;
import com.effectivemobile.test.kafka.KafkaService;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.ExampleObject;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.*;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/v1/comment")
@RequiredArgsConstructor
@Tag(name = " Controller",
        description = "CRUD operations with comment")
@ApiResponse(responseCode = "200")
public class CommentController {

    private final KafkaService kafkaService;

    @Value("${spring.kafka.gateway-to-comment-topic}")
    private String gatewayToTopic;

    @ApiResponse(responseCode = "200",
            content = @Content(examples = @ExampleObject(value = "{\"status\": \"UP\"}")))
    @GetMapping(value = "/health", produces = "application/json")
    public String getHealthStatus() throws ExecutionException, InterruptedException {

        KafkaMessage commentKafkaMessage = new KafkaMessage();
        commentKafkaMessage.setOperation("health");
        return kafkaService.sendMessage(gatewayToTopic, commentKafkaMessage).toString();
    }

    @FullSwaggerDescription(summary = "Get comment by id")
    @GetMapping(value = "/{id}", produces = "application/json")
    public String getById(@PathVariable(value = "id") @IdParamInfo Long id)
            throws ExecutionException, InterruptedException {

        KafkaMessage commentKafkaMessage = new KafkaMessage();
        commentKafkaMessage.setOperation("getCommentById");
        commentKafkaMessage.setTargetId(id.toString());

        return kafkaService.sendMessage(gatewayToTopic, commentKafkaMessage);
    }

    @FullSwaggerDescription(summary = "Change comments info")
    @PutMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    public String updateInfo(@PathVariable(value = "id") @IdParamInfo Long id,
                             @RequestBody String commentRq) throws ExecutionException, InterruptedException {

        KafkaMessage commentKafkaMessage = new KafkaMessage("updateCommentInfo", commentRq);
        commentKafkaMessage.setTargetId(id.toString());
        return kafkaService.sendMessage(gatewayToTopic, commentKafkaMessage);
    }


    @FullSwaggerDescription(summary = "Delete comment by id")
    @DeleteMapping(value = "/{id}", produces = "application/json")
    public String delete(@PathVariable(value = "id") @IdParamInfo Long id) throws ExecutionException, InterruptedException {

        KafkaMessage commentKafkaMessage = new KafkaMessage("deleteComment", null);
        commentKafkaMessage.setTargetId(id.toString());
        return kafkaService.sendMessage(gatewayToTopic, commentKafkaMessage);
    }

    @FullSwaggerDescription(summary = "Create new comment")
    @PostMapping(consumes = "application/json", produces = "application/json")
    public String create(@RequestBody String commentRq) throws ExecutionException, InterruptedException {

        KafkaMessage commentKafkaMessage = new KafkaMessage("createComment", commentRq);
        return kafkaService.sendMessage(gatewayToTopic, commentKafkaMessage);
    }
}
