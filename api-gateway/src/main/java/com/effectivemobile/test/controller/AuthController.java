package com.effectivemobile.test.controller;

import com.effectivemobile.test.service.AuthService;
import com.effectivemobile.test.security.JwtUtils;
import com.effectivemobile.test.dto.UserCredentials;
import com.fasterxml.jackson.core.JsonProcessingException;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.ExecutionException;

@RestController
@RequestMapping("/api/v1/auth")
@RequiredArgsConstructor
@Tag(name = " Controller",
        description = "CRUD operations with comment")
@ApiResponse(responseCode = "200")
public class AuthController {

    private final JwtUtils jwtUtils;
    private final AuthService authService;

    @PostMapping("/login")
    public String createAuthenticationToken(@RequestBody UserCredentials userCredentials)
            throws ExecutionException, InterruptedException, JsonProcessingException {

        UserCredentials authenticatedUserCredentials =
                authService.authenticate(userCredentials);

        return jwtUtils.generateToken(authenticatedUserCredentials);
    }
}
