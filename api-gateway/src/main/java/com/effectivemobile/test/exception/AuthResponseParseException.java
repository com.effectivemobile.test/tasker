package com.effectivemobile.test.exception;

public class AuthResponseParseException extends BadRequestException {

        public AuthResponseParseException(String message) {
            super(message);
        }
}
