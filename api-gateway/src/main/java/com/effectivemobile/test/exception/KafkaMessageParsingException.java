package com.effectivemobile.test.exception;

public class KafkaMessageParsingException extends BadRequestException {

        public KafkaMessageParsingException(String message) {
            super(message);
        }
}
