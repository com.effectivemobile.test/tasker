package com.effectivemobile.test.exception;

public class AuthenticationException extends BadRequestException {

        public AuthenticationException(String message) {
            super(message);
        }
}
