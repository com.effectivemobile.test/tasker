package com.effectivemobile.test.aspect;

import org.aspectj.lang.annotation.Pointcut;

@SuppressWarnings("unused")
public class LoggingPointcuts {
    @Pointcut("within(@org.springframework.web.bind.annotation.RestController *) || " +
            "within(@org.springframework.stereotype.Component *) || " +
            "within(@org.springframework.stereotype.Service *)")
    public void springBeanPointcut() {
    }

    @Pointcut("within(com.effectivemobile.test..*) && !within(com.effectivemobile.test.security.JwtRequestFilter)")
    public void applicationPackagePointcut() {
    }
}
