package com.effectivemobile.test.kafka;

import com.effectivemobile.test.exception.KafkaMessageParsingException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.UUID;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutionException;

@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaService {

    private final KafkaTemplate<String, KafkaMessage> kafkaTemplate;
    private final Map<String, CompletableFuture<String>> futuresMap = new ConcurrentHashMap<>();

    public String sendMessage(String topicName, KafkaMessage data) throws ExecutionException, InterruptedException {

        String correlationId = UUID.randomUUID().toString();
        CompletableFuture<String> futureResponse = new CompletableFuture<>();
        futuresMap.put(correlationId, futureResponse);

        data.setCorrelationId(correlationId);
        kafkaTemplate.send(topicName, data);

        return futureResponse.get();
    }

    @KafkaListener(
            topics = {"${spring.kafka.user-to-gateway-topic}",
                    "${spring.kafka.task-to-gateway-topic}",
                    "${spring.kafka.comment-to-gateway-topic}"},
            groupId = "${spring.kafka.group-id}",
            containerFactory = "kafkaMessageConcurrentKafkaListenerContainerFactory"
    )
    public void consume(@Payload KafkaMessage message) {
        String correlationId = message.getCorrelationId();
        CompletableFuture<String> futureResponse = futuresMap.remove(correlationId);
        futureResponse.complete(message.getMessage());
    }
}
