package com.effectivemobile.test.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Entity(name = "tasks")
public class Task {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Long id;

    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private TaskStatus status;

    @Column(name = "priority", nullable = false)
    @Enumerated(EnumType.STRING)
    private TaskPriority priority;

    @Column(name = "created_at", nullable = false)
    private LocalDateTime createdDate;

    @Column(name = "deadline")
    private LocalDateTime deadline;

    @Column(name = "author_id", nullable = false)
    private Long authorId;

    @OneToMany(mappedBy = "taskId", cascade = CascadeType.ALL)
    private List<TaskUserRelation> taskUserRelations;
}
