package com.effectivemobile.test.entity;

@SuppressWarnings("unused")
public enum TaskStatus {
    NEW,
    AWAITING,
    IN_PROGRESS,
    DONE
}
