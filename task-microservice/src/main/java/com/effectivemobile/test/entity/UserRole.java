package com.effectivemobile.test.entity;

@SuppressWarnings("unused")
public enum UserRole {
    AUTHOR, EXECUTOR
}
