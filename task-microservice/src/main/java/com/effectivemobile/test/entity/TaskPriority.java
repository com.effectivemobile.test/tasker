package com.effectivemobile.test.entity;

@SuppressWarnings("unused")
public enum TaskPriority {
    LOW,
    MEDIUM,
    HIGH
}
