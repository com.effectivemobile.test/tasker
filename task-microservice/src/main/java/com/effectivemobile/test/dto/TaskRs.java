package com.effectivemobile.test.dto;

import com.effectivemobile.test.entity.TaskPriority;
import com.effectivemobile.test.entity.TaskStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.util.List;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TaskRs {

    @Schema(example = "1")
    private Long id;

    @Schema(example = "Drink a coffee")
    private String title;

    @Schema(example = "1. Make a coffee 2. Add milk")
    private String description;

    @Schema(example = "DONE")
    private TaskStatus status;

    @Schema(example = "HIGH")
    private TaskPriority priority;

    @Schema(example = "2021-08-01T00:00:00")
    private String createdDate;

    @Schema(example = "1")
    private Long authorId;

    @Schema(example = "1, 2, 5, 17")
    private List<Long> executorsIds;
}
