package com.effectivemobile.test.dto;

import com.effectivemobile.test.entity.TaskPriority;
import com.effectivemobile.test.entity.TaskStatus;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Schema(description = "Change task info request")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class TaskRq {

    @Schema(example = "title")
    private String title;

    @Schema(example = "description")
    private String description;

    @Schema(example = "status")
    private TaskStatus status;

    @Schema(example = "priority")
    private TaskPriority priority;

    @Schema(example = "deadline")
    private LocalDateTime deadline;

    @Schema(example = "author_id")
    private Long authorId;

    public boolean isEmpty() {

        return (title == null || title.isEmpty()) &&
                (description == null || description.isEmpty()) &&
                status == null &&
                priority == null &&
                deadline == null &&
                authorId == null;
    }
}
