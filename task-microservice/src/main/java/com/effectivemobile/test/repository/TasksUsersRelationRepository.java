package com.effectivemobile.test.repository;

import com.effectivemobile.test.entity.TaskUserRelation;
import com.effectivemobile.test.entity.UserRole;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TasksUsersRelationRepository extends JpaRepository<TaskUserRelation, Long> {

    TaskUserRelation findByTaskIdAndUserRole(Long taskId, UserRole userRole);
}
