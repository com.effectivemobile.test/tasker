package com.effectivemobile.test.exception;

public class TaskInfoValidationException extends BadRequestException {

    public TaskInfoValidationException(String message) {
        super(message);
    }
}
