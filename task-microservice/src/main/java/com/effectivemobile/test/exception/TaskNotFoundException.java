package com.effectivemobile.test.exception;

public class TaskNotFoundException extends BadRequestException {
    public TaskNotFoundException(String message) {
        super(message);
    }
}
