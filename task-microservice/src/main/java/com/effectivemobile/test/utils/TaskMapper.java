package com.effectivemobile.test.utils;

import com.effectivemobile.test.dto.TaskRq;
import com.effectivemobile.test.dto.TaskRs;
import com.effectivemobile.test.entity.Task;
import org.mapstruct.Mapper;
import org.mapstruct.MappingConstants;
import org.mapstruct.factory.Mappers;

@SuppressWarnings("unused")
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface TaskMapper {
    TaskMapper INSTANCE = Mappers.getMapper(TaskMapper.class);

    TaskRs toTaskRs(Task task);

    Task toTask(TaskRq taskRq);
}
