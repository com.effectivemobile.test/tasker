package com.effectivemobile.test.service;

import com.effectivemobile.test.annotation.Debug;
import com.effectivemobile.test.dto.CommonRs;
import com.effectivemobile.test.dto.TaskRq;
import com.effectivemobile.test.dto.TaskRs;
import com.effectivemobile.test.entity.Task;
import com.effectivemobile.test.entity.TaskUserRelation;
import com.effectivemobile.test.entity.UserRole;
import com.effectivemobile.test.exception.BadRequestException;
import com.effectivemobile.test.exception.TaskInfoValidationException;
import com.effectivemobile.test.exception.TaskNotFoundException;
import com.effectivemobile.test.repository.TaskRepository;
import com.effectivemobile.test.repository.TasksUsersRelationRepository;
import com.effectivemobile.test.utils.TaskMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Debug
@Service
@RequiredArgsConstructor
public class TaskService {

    private final TaskRepository taskRepository;
    private final TaskMapper taskMapper;
    private final TasksUsersRelationRepository tasksUsersRelationRepository;

    public CommonRs<TaskRs> getTaskById(Long id) {

        Task task = extractTaskFromDbById(id);

        return new CommonRs<>(taskMapper.toTaskRs(task));
    }

    public CommonRs<TaskRs> updateTaskInfo(Long id, TaskRq taskRq) {

        validateTaskUpdateRequest(taskRq);

        Task task = extractTaskFromDbById(id);

        if (taskRq.getTitle() != null) {
            task.setTitle(taskRq.getTitle());
        }
        if (taskRq.getDescription() != null) {
            task.setDescription(taskRq.getDescription());
        }
        if (taskRq.getStatus() != null) {
            task.setStatus(taskRq.getStatus());
        }
        if (taskRq.getPriority() != null) {
            task.setPriority(taskRq.getPriority());
        }
        if (taskRq.getDeadline() != null) {
            task.setDeadline(taskRq.getDeadline());
        }
        if (taskRq.getAuthorId() != null &&
                !taskRq.getAuthorId().equals(task.getAuthorId())) {
            updateAuthorIdInTaskUserRelation(id, taskRq.getAuthorId());
            task.setAuthorId(taskRq.getAuthorId());
        }

        taskRepository.save(task);

        return new CommonRs<>(taskMapper.toTaskRs(task));
    }

    public CommonRs<String> deleteTask(Long id) {

        Task task = extractTaskFromDbById(id);

        taskRepository.delete(task);

        return new CommonRs<>("Task with id " + id + " successfully deleted");
    }

    public CommonRs<TaskRs> createTask(TaskRq taskRq) {

        validateTaskCreateRequest(taskRq);

        Task task = taskMapper.toTask(taskRq);
        task.setCreatedDate(LocalDateTime.now());

        Task savedTask = taskRepository.save(task);

        TaskUserRelation taskUserRelation = new TaskUserRelation();
        taskUserRelation.setTaskId(savedTask.getId());
        taskUserRelation.setUserId(taskRq.getAuthorId());
        taskUserRelation.setUserRole(UserRole.AUTHOR);

        tasksUsersRelationRepository.save(taskUserRelation);

        return new CommonRs<>(taskMapper.toTaskRs(savedTask));
    }

    private Task extractTaskFromDbById(Long id) {

        return taskRepository.findById(id)
                .orElseThrow(() -> new TaskNotFoundException("Task with id " + id + " not found"));
    }

    private void validateTaskUpdateRequest(TaskRq taskRq) {

        if (taskRq == null || taskRq.isEmpty()) {
            throw new BadRequestException("Task update request body is empty");
        }

        String errorMessage = "Task update request validation failed: ";
        boolean validationFailed = false;

        if (taskRq.getDeadline() != null &&
                taskRq.getDeadline().isBefore(LocalDateTime.now())) {
            errorMessage += "deadline should be in future! ";
            validationFailed = true;
        }

        if (validationFailed) {
            throw new TaskInfoValidationException(errorMessage);
        }

        //TODO add authorId validation
    }

    private void validateTaskCreateRequest(TaskRq taskRq) {

        String errorMessage = "Task create request validation failed: ";
        boolean validationFailed = false;

        if (taskRq == null || taskRq.isEmpty()) {
            throw new BadRequestException("Task create request body should not be empty");
        }

        if (taskRq.getTitle() == null) {
            errorMessage += "| Title is reqired! | ";
            validationFailed = true;
        }

        if (taskRq.getStatus() == null) {
            errorMessage += "| Status is required! | ";
            validationFailed = true;
        }

        if (taskRq.getPriority() == null) {
            errorMessage += "| Priority is required! | ";
            validationFailed = true;
        }
        if (taskRq.getDeadline() != null &&
                taskRq.getDeadline().isBefore(LocalDateTime.now())) {
            errorMessage += "| Deadline should be in future! | ";
            validationFailed = true;
        }
        if (taskRq.getAuthorId() == null) {
            errorMessage += "| Author id is required! | ";
            validationFailed = true;
        }

        if (validationFailed) {
            throw new TaskInfoValidationException(errorMessage);
        }
    }

    private void updateAuthorIdInTaskUserRelation(Long taskId, Long newAuthorId) {
        TaskUserRelation taskUserRelation = tasksUsersRelationRepository
                .findByTaskIdAndUserRole(taskId, UserRole.AUTHOR);
        taskUserRelation.setUserId(newAuthorId);
        tasksUsersRelationRepository.save(taskUserRelation);
    }
}
