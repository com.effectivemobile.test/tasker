package com.effectivemobile.test.kafka;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;


@NoArgsConstructor
@AllArgsConstructor
@Data
@Builder
public class KafkaMessage {

    private String correlationId;
    private String targetId;
    private String operation;
    private String message;
}
