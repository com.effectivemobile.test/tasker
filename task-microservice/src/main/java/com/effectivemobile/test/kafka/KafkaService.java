package com.effectivemobile.test.kafka;

import com.effectivemobile.test.dto.CommonRs;
import com.effectivemobile.test.dto.ErrorRs;
import com.effectivemobile.test.dto.TaskRq;
import com.effectivemobile.test.dto.TaskRs;
import com.effectivemobile.test.exception.BadRequestException;
import com.effectivemobile.test.exception.KafkaSendMessageException;
import com.effectivemobile.test.service.TaskService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
public class KafkaService {

    private final KafkaTemplate<String, KafkaMessage> kafkaTemplate;
    private final TaskService taskService;
    private final ObjectMapper objectMapper;

    public void sendMessage(String topicName, KafkaMessage data) {

        CompletableFuture<SendResult<String, KafkaMessage>> future =
                kafkaTemplate.send(topicName, data);

        future.whenComplete((result, exception) -> {
            if (exception != null) {
                throw new KafkaSendMessageException("Unable to send message=[" +
                        data + "] due to : " + exception.getMessage());
            }
        });
    }

    @KafkaListener(
            topics = "${spring.kafka.gateway-to-task-topic}",
            groupId = "${spring.kafka.group-id}",
            containerFactory = "kafkaMessageConcurrentKafkaListenerContainerFactory"
    )
    public void consumeGatewayMessage(@Payload KafkaMessage message) throws JsonProcessingException {

        long methodTargetId = -1L;
        if(message.getTargetId() != null) {
            methodTargetId = Long.parseLong(message.getTargetId());
        }

        String response;

        try {
            switch (message.getOperation()) {

                case "health":
                    String[] healthStatus = {"status", "UP"};
                    response = objectMapper.writeValueAsString(healthStatus);
                    break;
                case "getTaskById":
                    CommonRs<TaskRs> user = taskService.getTaskById(methodTargetId);
                    response = objectMapper.writeValueAsString(user);
                    break;
                case "updateTaskInfo":
                    TaskRq taskRq = getTaskRqFromMessage(message);
                    CommonRs<TaskRs> updatedUserInfo = taskService.updateTaskInfo(methodTargetId, taskRq);
                    response = objectMapper.writeValueAsString(updatedUserInfo);
                    break;
                case "deleteTask":
                    CommonRs<String> deletedUser = taskService.deleteTask(methodTargetId);
                    response = objectMapper.writeValueAsString(deletedUser);
                    break;
                case "createTask":
                    TaskRq newTaskRq = getTaskRqFromMessage(message);
                    CommonRs<TaskRs> newTask = taskService.createTask(newTaskRq);
                    response = objectMapper.writeValueAsString(newTask);
                    break;
                default:
                    message.setOperation("EXCEPTION");
                    ErrorRs errorRs = new ErrorRs(new BadRequestException("Unknown operation"));
                    response = objectMapper.writeValueAsString(errorRs);
                    break;
            }
        } catch (BadRequestException exception) {
            message.setOperation("EXCEPTION");
            response = objectMapper.writeValueAsString(new ErrorRs(exception));
        }

        if (response == null) {

            response = objectMapper.writeValueAsString(new ErrorRs(
                    new BadRequestException("Bad operation request")));
        }

        message.setMessage(response);
        sendMessage("task_to_gateway_topic", message);
    }

    private TaskRq getTaskRqFromMessage(KafkaMessage message) {

        TaskRq newTaskRq;
        try {
            newTaskRq = objectMapper.readValue(message.getMessage(), TaskRq.class);
        } catch (JsonProcessingException exception) {
            throw new BadRequestException("Unable to parse request body");
        }

        return newTaskRq;
    }
}
