package com.effectivemobile.test.aspect;

import org.aspectj.lang.annotation.Pointcut;

@SuppressWarnings("unused")
public class AnnotationPointcuts {
    @Pointcut("(@target(com.effectivemobile.test.annotation.Info)) && " +
            "!@annotation(com.effectivemobile.test.annotation.NotLoggable)")
    public void info() {
    }

    @Pointcut("(@target(com.effectivemobile.test.annotation.Debug)) &&" +
            "!@annotation(com.effectivemobile.test.annotation.NotLoggable)")
    public void debug() {
    }
}
