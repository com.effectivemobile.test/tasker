package com.effectivemobile.test.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@Schema(description = "Change user info request")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserRq {

    @Schema(example = "John")
    private String firstName;

    @Schema(example = "Doe")
    private String lastName;

    @Schema(example = "JDMaster")
    private String nickname;

    @Schema(example = "I love cars")
    private String about;

    @Schema(example = "jdmaster@email.com")
    private String email;

    @Schema(example = "Super-secret12345!")
    private String password;

    @Schema(example = "2000-08-01T00:00:00")
    private LocalDateTime birthDate;

    public boolean isNotEmpty() {
        if (firstName != null && !firstName.isEmpty())
            return true;
        if (lastName != null && !lastName.isEmpty())
            return true;
        if (nickname != null && !nickname.isEmpty())
            return true;
        if (about != null && !about.isEmpty())
            return true;
        if (email != null && !email.isEmpty())
            return true;

        return birthDate != null;
    }
}
