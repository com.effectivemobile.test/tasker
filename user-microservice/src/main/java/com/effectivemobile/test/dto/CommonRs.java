package com.effectivemobile.test.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.PropertyNamingStrategies;
import com.fasterxml.jackson.databind.annotation.JsonNaming;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Data;

import java.time.LocalDateTime;
import java.time.ZoneOffset;

@Data
@JsonNaming(PropertyNamingStrategies.SnakeCaseStrategy.class)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class CommonRs<T> {

    @Schema(example = "12432857239")
    private Long timeStamp;

    @Schema(example = "Collection of objects or just object any type")
    private T data;

    @Schema(example = "20")
    private Integer itemPerPage;

    @Schema(example = "0")
    private Integer offset;

    @Schema(example = "20")
    private Integer perPage;

    @Schema(example = "100")
    private Long total;

    public CommonRs(T data) {
        this.data = data;
        this.timeStamp = LocalDateTime.now().toEpochSecond(ZoneOffset.UTC);
    }
}
