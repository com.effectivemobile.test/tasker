package com.effectivemobile.test.service;

import com.effectivemobile.test.annotation.Debug;
import com.effectivemobile.test.dto.CommonRs;
import com.effectivemobile.test.dto.UserCredentials;
import com.effectivemobile.test.dto.UserRq;
import com.effectivemobile.test.dto.UserRs;
import com.effectivemobile.test.entity.User;
import com.effectivemobile.test.exception.BadRequestException;
import com.effectivemobile.test.exception.UserDataValidationException;
import com.effectivemobile.test.exception.UserNotFoundException;
import com.effectivemobile.test.kafka.KafkaMessage;
import com.effectivemobile.test.repository.UserRepository;
import com.effectivemobile.test.utils.UserMapper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Debug
@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final ObjectMapper objectMapper;

    public CommonRs<UserRs> getUserById(Long id) {
        User user = extractUserFromDb(id);

        return new CommonRs<>(userMapper.toUserRs(user));
    }

    public CommonRs<UserRs> createUser(UserRq userRq) {

        validateUserCreationRequest(userRq);

        User user = userMapper.toUser(userRq);

        user.setRegistrationDate(LocalDateTime.now());
        userRepository.save(user);

        userMapper.toUserRs(user);
        return new CommonRs<>(userMapper.toUserRs(user));
    }

    public CommonRs<UserRs> updateUserInfo(Long id, UserRq userRq) {

        validateUserUpdateRequest(userRq);

        User user = extractUserFromDb(id);

        if (userRq.getFirstName() != null) {
            user.setFirstName(userRq.getFirstName());
        }
        if (userRq.getLastName() != null) {
            user.setLastName(userRq.getLastName());
        }
        if (userRq.getNickname() != null) {
            user.setNickname(userRq.getNickname());
        }
        if (userRq.getAbout() != null) {
            user.setAbout(userRq.getAbout());
        }
        if (userRq.getEmail() != null) {
            user.setEmail(userRq.getEmail());
        }
        if (userRq.getBirthDate() != null) {
            user.setBirthDate(userRq.getBirthDate());
        }

        User savedUser = userRepository.save(user);

        return new CommonRs<>(userMapper.toUserRs(savedUser));
    }

    public CommonRs<String> deleteUser(Long id) {

        User user = extractUserFromDb(id);

        userRepository.delete(user);

        return new CommonRs<>("User with id: " + id + " deleted");
    }

    public KafkaMessage authenticate(UserCredentials userCredentials) throws JsonProcessingException {

        KafkaMessage response = new KafkaMessage();

        User user = userRepository.findByEmail(userCredentials.getUsername());
        if (user == null) {
            response.setMessage("FAIL");
            return response;
        }

        if (user.getPassword().equals(userCredentials.getPassword())) {
            userCredentials.setId(user.getId().toString());
            userCredentials.setPassword(null);
            response.setMessage("SUCCESS;" + objectMapper.writeValueAsString(userCredentials));
            return response;
        } else {
            response.setMessage("FAIL");
            return response;
        }
    }

    private User extractUserFromDb(Long id) {

        return userRepository.findById(id).orElseThrow(
                () -> new UserNotFoundException("User with id " + id + " not found"));
    }

    private void validateUserUpdateRequest(UserRq userRq) {

        if (userRq == null || !userRq.isNotEmpty()) {
            throw new BadRequestException("User update request body is empty");
        }

        String errorMessage = "User data validation failed: ";
        boolean validationFailed = false;

        if (userRq.getNickname() != null &&
                (userRq.getNickname().length() < 5 || userRq.getNickname().length() > 20)) {
            errorMessage += "| Nickname length should be between 5 and 20 characters! | ";
            validationFailed = true;
        }

        if (userRepository.existsByEmailIgnoreCase(userRq.getEmail())) {
            errorMessage += "| User with email: " + userRq.getEmail() + " already exists! | ";
            validationFailed = true;
        }

        if (userRq.getBirthDate() != null && birthDateIsNotValid(userRq.getBirthDate())) {
            errorMessage += "| Birth date should be in the past! | ";
            validationFailed = true;
        }

        if (validationFailed) {
            throw new UserDataValidationException(errorMessage);
        }
    }

    private void validateUserCreationRequest(UserRq userRq) {

        if (userRq == null) {
            throw new BadRequestException("User creation request body is empty");
        }

        String errorMessage = "User data validation failed: ";
        boolean validationFailed = false;

        if (userRq.getNickname() == null || userRq.getNickname().isEmpty()) {
            errorMessage += "| Nickname is required! | ";
            validationFailed = true;
        } else if (userRq.getNickname().length() < 3 || userRq.getNickname().length() > 20) {
            errorMessage += "| Nickname length should be longer between 5 and 20 characters! | ";
            validationFailed = true;
        } else if (userRepository.existsByNicknameIgnoreCase(userRq.getNickname())) {
            errorMessage += "| User with nickname: " + userRq.getNickname() + " already exists! | ";
            validationFailed = true;
        }

        if (userRq.getEmail() == null || userRq.getEmail().isEmpty()) {
            errorMessage += "| Email is required! | ";
            validationFailed = true;
        }
        if (userRq.getPassword() == null || userRq.getPassword().isEmpty()) {
            errorMessage += "| Password is required! | ";
            validationFailed = true;
        }
        if (birthDateIsNotValid(userRq.getBirthDate())) {
            errorMessage += "| Birth date should be in the past! | ";
            validationFailed = true;
        }

        if (userRepository.existsByEmailIgnoreCase(userRq.getEmail())) {
            errorMessage += "| User with email: " + userRq.getEmail() + " already exists! | ";
            validationFailed = true;
        }

        if (validationFailed) {
            throw new UserDataValidationException(errorMessage);
        }
    }

    private boolean birthDateIsNotValid(LocalDateTime birthDate) {
        return !birthDate.isBefore(LocalDateTime.now());
    }

    public KafkaMessage getCredentialsByUserName(String userName) throws JsonProcessingException {

        User user = userRepository.findByEmailIgnoreCase(userName);

        KafkaMessage responseMessage = new KafkaMessage();
        if (user == null) {
            responseMessage.setOperation("EXCEPTION");
            responseMessage.setMessage("not found");
            return responseMessage;
        }

        UserCredentials userCredentials = new UserCredentials();
        userCredentials.setId(user.getId().toString());
        userCredentials.setUsername(user.getEmail());

        responseMessage.setMessage(objectMapper.writeValueAsString(userCredentials));

        return responseMessage;
    }
}
