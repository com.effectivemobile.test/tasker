package com.effectivemobile.test.repository;

import com.effectivemobile.test.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    boolean existsByEmailIgnoreCase(String email);

    boolean existsByNicknameIgnoreCase(String nickname);

    User findByEmail(String email);

    User findByEmailIgnoreCase(String email);
}
