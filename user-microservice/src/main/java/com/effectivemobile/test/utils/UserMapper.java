package com.effectivemobile.test.utils;

import com.effectivemobile.test.dto.UserRq;
import com.effectivemobile.test.dto.UserRs;
import com.effectivemobile.test.entity.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.MappingConstants;
import org.mapstruct.factory.Mappers;

@SuppressWarnings("unused")
@Mapper(componentModel = MappingConstants.ComponentModel.SPRING)
public interface UserMapper {
    UserMapper INSTANCE = Mappers.getMapper(UserMapper.class);

    UserRs toUserRs(User user);

    @Mapping(target = "id", ignore = true)
    User toUser(UserRq userRq);
}
