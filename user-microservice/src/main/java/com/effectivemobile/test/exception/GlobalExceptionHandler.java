package com.effectivemobile.test.exception;

import com.effectivemobile.test.dto.ErrorRs;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

@ControllerAdvice
@RequiredArgsConstructor
public class GlobalExceptionHandler {

    @ExceptionHandler({BadRequestException.class})
    public ResponseEntity<ErrorRs> handleCommonExceptions(BadRequestException exception) {

        return ResponseEntity.badRequest().body(new ErrorRs(exception));
    }
}