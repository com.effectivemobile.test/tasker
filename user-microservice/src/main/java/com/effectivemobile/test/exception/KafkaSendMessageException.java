package com.effectivemobile.test.exception;

public class KafkaSendMessageException extends RuntimeException {

    public KafkaSendMessageException(String message) {
        super(message);
    }
}
