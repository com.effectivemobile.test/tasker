package com.effectivemobile.test.exception;

public class UserDataValidationException extends BadRequestException {
    public UserDataValidationException(String message) {
        super(message);
    }
}
