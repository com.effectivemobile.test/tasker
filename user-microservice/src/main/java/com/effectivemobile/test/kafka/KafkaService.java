package com.effectivemobile.test.kafka;

import com.effectivemobile.test.dto.*;
import com.effectivemobile.test.exception.BadRequestException;
import com.effectivemobile.test.exception.KafkaSendMessageException;
import com.effectivemobile.test.service.UserService;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
@RequiredArgsConstructor
@Slf4j
public class KafkaService {

    private final KafkaTemplate<String, KafkaMessage> kafkaTemplate;
    private final UserService userService;
    private final ObjectMapper objectMapper;

    public void sendMessage(String topicName, KafkaMessage data) {

        CompletableFuture<SendResult<String, KafkaMessage>> future =
                kafkaTemplate.send(topicName, data);
        log.debug("Sending message: " + data);

        future.whenComplete((result, exception) -> {
            if (exception != null) {
                throw new KafkaSendMessageException("Unable to send message=[" +
                        data + "] due to : " + exception.getMessage());
            }
        });
    }

    @KafkaListener(
            topics = "${spring.kafka.gateway-to-user-topic}",
            groupId = "${spring.kafka.group-id}",
            containerFactory = "kafkaMessageConcurrentKafkaListenerContainerFactory"
    )
    public void consume(@Payload KafkaMessage message) throws JsonProcessingException {

        long methodTargetId = -1L;
        if (message.getTargetId() != null) {
            methodTargetId = Long.parseLong(message.getTargetId());
        }

        String response = null;

        try {
            switch (message.getOperation()) {
                case "health":
                    String[] healthStatus = {"status", "UP"};
                    response = objectMapper.writeValueAsString(healthStatus);
                    break;
                case "getUserById":
                    CommonRs<UserRs> user = userService.getUserById(methodTargetId);
                    response = objectMapper.writeValueAsString(user);
                    break;
                case "updateUserInfo":
                    UserRq userRq = getUserRqFromMessage(message);
                    CommonRs<UserRs> updatedUserInfo = userService.updateUserInfo(methodTargetId, userRq);
                    response = objectMapper.writeValueAsString(updatedUserInfo);
                    break;
                case "deleteUser":
                    CommonRs<String> deletedUser = userService.deleteUser(methodTargetId);
                    response = objectMapper.writeValueAsString(deletedUser);
                    break;
                case "createUser":
                    UserRq newUserRq = getUserRqFromMessage(message);
                    CommonRs<UserRs> newUser = userService.createUser(newUserRq);
                    response = objectMapper.writeValueAsString(newUser);
                    break;
                case "authenticate":
                    UserCredentials userCredentials =
                            objectMapper.readValue(message.getMessage(), UserCredentials.class);
                    KafkaMessage authResponseMessage = userService.authenticate(userCredentials);
                    authResponseMessage.setCorrelationId(message.getCorrelationId());
                    sendMessage("user_to_gateway_topic", authResponseMessage);
                    return;
                case "getCredentialsByUserName":
                    String userName = message.getMessage();
                    KafkaMessage credentialsResponseMessage = userService.getCredentialsByUserName(userName);
                    credentialsResponseMessage.setCorrelationId(message.getCorrelationId());
                    sendMessage("user_to_gateway_topic", credentialsResponseMessage);
                    break;
                default:
                    message.setOperation("EXCEPTION");
                    ErrorRs errorRs = new ErrorRs(new BadRequestException("Unknown operation"));
                    response = objectMapper.writeValueAsString(errorRs);
                    break;
            }
        } catch (BadRequestException exception) {
            message.setOperation("EXCEPTION");
            response = objectMapper.writeValueAsString(new ErrorRs(exception));
        }

        if (response == null) {

            response = objectMapper.writeValueAsString(new ErrorRs(
                    new BadRequestException("Bad operation request")));
        }

        message.setMessage(response);
        sendMessage("user_to_gateway_topic", message);
    }

    private UserRq getUserRqFromMessage(KafkaMessage message) {
        UserRq userRq;
        try {
            userRq = objectMapper.readValue(message.getMessage(), UserRq.class);
        } catch (JsonProcessingException exception) {
            throw new BadRequestException("Unable to parse user request body");
        }

        return userRq;
    }
}
